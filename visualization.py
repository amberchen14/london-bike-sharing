# -*- coding: utf-8 -*-
"""
Created on Sun Nov 17 20:28:35 2019

@author: Amber
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from datetime import datetime
from sklearn.utils import shuffle
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import cross_val_score
from sklearn import metrics
import plotly.express as px
import plotly as plot
import plotly.graph_objs as go
from plotly.offline import plot
from plotly.subplots import make_subplots


#label_layout=label_format(min_value+interval,max_value, interval)    
#def label_format (min_value, max_value, interval):
#    Label=['-Inf-{}'.format(min_value)]
#    subLabel1=['{}-{}'.format(x, x+5)for x in list(range(min_value, max_value, interval))]
#    subLabel2=['{}-Inf'.format(max_value)]
#    Label.extend(subLabel1)
#    Label.extend(subLabel2)
#    return Label

def temp_dist(raw_data):
    t1_dist=raw_data[['t1']]
    t1_dist=t1_dist.rename(columns={'t1':'t'})
    t1_dist['type']='Temperature'
    t2_dist=raw_data[['t2']]
    t2_dist=t2_dist.rename(columns={'t2':'t'})
    t2_dist['type']='Feel Temperature'
    t_dist=t1_dist.append(t2_dist)
    if t_dist['t'].dtypes==float:
        t_dist['t']=t_dist['t'].round(1)
    return t_dist
   
raw=pd.read_csv("london_merged.csv")
#shape
print(raw.shape)
#description
print(raw.describe())
#check if any cell is nan
raw.isna().values.any()

#data process: weather
#"weathe_code" category description on Kaggle:
#1 = Clear ; mostly clear but have some values with haze/fog/patches of fog/ fog in vicinity
#2 = scattered clouds / few clouds
#3 = Broken clouds
#4 = Cloudy
#7 = Rain/ light Rain shower/ Light rain
#10 = rain with thunderstorm
#26 = snowfall
#94 = Freezing Fog
weather_info=pd.DataFrame({'code':[1, 2, 3, 4, 7, 10, 26, 94], 
                          'weather':['Clear', 
                          'Scatter', 
                          'Broken', 
                          'Cloudy',
                          'Rainy', 
                          'Thunder', 
                          'Snowfall', 
                          'Freeze']})
raw=pd.merge(raw, weather_info, how='left',left_on='weather_code', right_on='code' )

#data process: timestamp
raw['timestamp']=pd.to_datetime(raw.timestamp)
raw['hour']=pd.DatetimeIndex(raw['timestamp']).hour
raw['month']=pd.DatetimeIndex(raw['timestamp']).month
raw['year']=pd.DatetimeIndex(raw['timestamp']).year
#data time range
print(raw['timestamp'].describe())

# data visualization:
#plt.style.use(['seaborn'])
sns.set_style("ticks")
#weather distribution

## This plot shows that average usages at weather Scatter Cloud and Broken Cloud are above average.
## One interesting thing is weather Clear is below the average. We expect weather Cloud may have higher usage.
## Another interesting thing is weather Thunderstorm is close to average. 
weather_count=raw.groupby('weather').size().to_frame('hour').sort_values(by=['hour'], ascending=False).reset_index()
explode = (0, 0, 0, 0, 0, 0.1, 0.8)
fig, ax = plt.subplots(1,2, figsize=(16,8))
fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.2, hspace=None)
ax[0].pie(x=weather_count['hour'],
          labels=weather_count['weather'],
          autopct='%1.1f%%',
          colors=sns.color_palette('Paired'),
          explode=explode,startangle = 90
          ) #[0:weather_count.shape[0]]
ax[0].axis('equal')
sns.catplot(x='weather', y='cnt',
            kind="box",
            data=raw,
            order=weather_count['weather'], 
            palette='Paired',
            ax=ax[1]
    )
ax[1].set_yscale('log')
ax[1].axhline(float(raw[['cnt']].quantile(q=0.25)), c='red', label="25%")
ax[1].axhline(float(raw[['cnt']].quantile(q=0.5)), ls='--',  c='red',label="50%")
ax[1].axhline(float(raw[['cnt']].quantile(q=0.75)), ls=':', linewidth=4, c='red',label="75%")
labels = [ "25%", '50%', '75%']
handles, _ = ax[1].get_legend_handles_labels()
ax[1].legend(handles = handles, labels = labels)
plt.close(2)

#t1 and t2 raw distribution. The plots show that 0.5 degree x-axis has less hours than integer degree. 
#A weird temperature degree at 10.3 
temp_raw_data=raw[['t1', 't2']]
t_dist=temp_dist(temp_raw_data)
g=sns.catplot(data=t_dist,
              x='t',
              row='type',
              hue='type',
              kind='count',
              height=3.5,
              aspect=4
              )
g.set_axis_labels("Celsius", "Hour")
g.set_titles("{row_name}")
g.despine(left=True)
for ax in g.axes.flat:
    for label in ax.get_xticklabels():
        label.set_rotation(90)

#cut t1 t2 with interval 1. Replot.
#t1 is similar to One Normal distribution. However, t2 looks like two distirbution. Unintuitive data for me. 
min_value=-10
max_value=35
interval=1
raw['t1_cut1']=pd.cut(raw['t1'],
                        bins=pd.interval_range(start=min_value, 
                                               end=max_value, 
                                               freq=interval, 
                                               closed='left')#,
                    )
raw['t2_cut1']=pd.cut(raw['t2'],
                        bins=pd.interval_range(start=min_value, 
                                               end=max_value, 
                                               freq=interval, 
                                               closed='left')#,
                    )

raw_data=raw[['t1_cut1', 't2_cut1']]
raw_data=raw_data.rename(columns={'t1_cut1':'t1', 't2_cut1':'t2'})
t_dist=temp_dist(raw_data)

g=sns.catplot(data=t_dist,
              x='t',
              row='type',
              hue='type',
              kind='count',
              height=3.5,
              aspect=4
              )
g.set_axis_labels("Celsius", "Hour")
g.set_titles("{row_name}")
for ax in g.axes.flat:
    for label in ax.get_xticklabels():
        label.set_rotation(90)

## The left plot shows the correlation between t1 and cnt. 
## Average tempture less than 15 degree is less than average cnt.
## In addition, the right figure shows the number of hours is top at weather Clear when t1 is less than 1
##Plot the relationship between cnt and t1
        
##The plot shows that when speed is less than 
min_value=-10
max_value=35
interval=5
raw['t1_cut5']=pd.cut(raw['t1'],
                        bins=pd.interval_range(start=min_value, end=max_value, freq=interval, closed='left')#,
                        #labels=label_layout
                    )
#This plot shows average count is less than 50 when temperature is less than 15.  
fig, ax = plt.subplots( figsize=(16,8))
sns.boxplot(
    data=raw, 
    x='t1_cut1',
    y='cnt',
    color='white',
    ax=ax
    )
ax.set(xlabel='Temperature')
ax.axhline(float(raw[['cnt']].quantile(q=0.25)), c='red', label="25%")
ax.axhline(float(raw[['cnt']].quantile(q=0.5)), ls='--',  c='red',label="50%")
ax.axhline(float(raw[['cnt']].quantile(q=0.75)), ls=':', linewidth=4, c='red',label="75%")
for label in ax.get_xticklabels():
    label.set_rotation(90)
labels = [ "25%", '50%', '75%']
handles, _ = ax.get_legend_handles_labels()
ax.legend(handles = handles, labels = labels)

fig = make_subplots(rows=2, cols=4,specs=[[{"type": "domain"},
                                          {"type": "domain"}, 
                                          {"type": "domain"}, 
                                          {"type": "domain"}],
                                          [{"type": "domain"},
                                          {"type": "domain"},
                                          {"type": "domain"},
                                          {"type": "domain"}]
                                          ],
                    subplot_titles=weather_count['weather'])
for i in list(range(weather_count.shape[0])):
    name=weather_count['weather'][i]
    df=raw[(raw['weather']== name)]
    df=df.rename(columns={'t1_cut5':'Temperature'})
    labels = df.Temperature.value_counts(sort=False).index.astype(str)
    values = df.Temperature.value_counts(sort=False).values
    r=int(i/4)+1
    c=(i-4*int(i/4)+1)
    fig.add_trace(go.Pie(labels=labels, values=values, name=name, textinfo='label+percent'),
                  row=r, col=c)
fig.update_layout(showlegend=False)
plot(fig, auto_open=True) 

#Hour and cnt distribution
g=sns.catplot(data=raw,
              x='hour',
              y='cnt',
              col='is_holiday',
              row='is_weekend',
              kind="box",
              height=4,
              aspect=1.5
              )
labels = [ "25%", '50%', '75%']
for ax in g.axes.flat:
    ax.axhline(float(raw[['cnt']].quantile(q=0.25)), c='red', label="25%")
    ax.axhline(float(raw[['cnt']].quantile(q=0.5)), ls='--',  c='red',label="50%")
    ax.axhline(float(raw[['cnt']].quantile(q=0.75)), ls=':', linewidth=4, c='red',label="75%")
    handles, _ = ax.get_legend_handles_labels()
    ax.legend(handles = handles, labels = labels)

#month and cnt distribution
g=sns.catplot(data=raw,
              x='month',
              y='cnt',
              kind="box",
              height=4,
              aspect=1.5
              )
labels = [ "25%", '50%', '75%']
for ax in g.axes.flat:
    ax.axhline(float(raw[['cnt']].quantile(q=0.25)), c='red', label="25%")
    ax.axhline(float(raw[['cnt']].quantile(q=0.5)), ls='--',  c='red',label="50%")
    ax.axhline(float(raw[['cnt']].quantile(q=0.75)), ls=':', linewidth=4, c='red',label="75%")
    handles, _ = ax.get_legend_handles_labels()
    ax.legend(handles = handles, labels = labels)
















                            





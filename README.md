# Analysis of London-Bike-Sharing
Investigating London bike sharing usages with data from [Kaggle](https://www.kaggle.com/hmavrodiev/london-bike-sharing-dataset).The end goal is to create some cool graphics and to develop a linear regression model of the usage prediction based on the previous 23 hours data.

## File description
london_merge.csv: raw data <br>
visualization.py/ipynb: create some cool plots using matplotlib, seaborn, and plotly.<br>
ML.py: develop linear regression models in two different ways with scikit learn. The final model's score is 0.93.


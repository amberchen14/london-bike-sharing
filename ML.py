#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 16:25:34 2020

@author: amberchen
"""

# -*- coding: utf-8 -*-
"""
Created on Sun Nov 17 20:28:35 2019

@author: Amber
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from datetime import datetime
from sklearn.utils import shuffle
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import cross_val_score
from sklearn import metrics

raw=pd.read_csv("london_merged.csv")
#data process: weather
#"weathe_code" category description on Kaggle:
#1 = Clear ; mostly clear but have some values with haze/fog/patches of fog/ fog in vicinity
#2 = scattered clouds / few clouds
#3 = Broken clouds
#4 = Cloudy
#7 = Rain/ light Rain shower/ Light rain
#10 = rain with thunderstorm
#26 = snowfall
#94 = Freezing Fog
weather_info=pd.DataFrame({'code':[1, 2, 3, 4, 7, 10, 26, 94], 
                          'weather':['Clear', 
                          'Scatter clouds', 
                          'Broken clouds', 
                          'Cloudy',
                          'Rainy', 
                          'Thunderstorm', 
                          'Snowfall', 
                          'Freeze fog']})

raw=pd.merge(raw, weather_info, how='left',left_on='weather_code', right_on='code' )

#data process: timestamp
raw['timestamp']=pd.to_datetime(raw.timestamp)
raw['hour']=pd.DatetimeIndex(raw['timestamp']).hour
raw['month']=pd.DatetimeIndex(raw['timestamp']).month
raw['year']=pd.DatetimeIndex(raw['timestamp']).year


raw['Snowfall']=0
raw.loc[raw['weather']=='Snowfall','Snowfall']=1

raw['Rainy']=0
raw.loc[raw['weather']=='Rainy','Rainy']=1

raw['Freeze fog']=0
raw.loc[raw['Freeze fog']=='Freeze fog','Freeze fog']=1

#Chose variable
#'Snowfall','Rainy','Freeze fog',
data_ml=raw.filter(['hour','wind_speed','hum', 'is_weekend', 'is_holiday', 't1', 'cnt'])
#preprocess
array_ml=data_ml.to_numpy()
previous_hours=23
x = np.empty(shape = (data_ml.shape[0]-previous_hours ,previous_hours*data_ml.shape[1]),dtype = float)
y = np.empty(shape = (data_ml.shape[0]-previous_hours, 1),dtype = float)
y_data=data_ml.iloc[previous_hours:,:]
for i in range(0, raw.shape[0]-previous_hours):
    x[i, :]=array_ml[i:previous_hours+i,: ].reshape(1,-1)
    y[i]=array_ml[previous_hours+i, data_ml.shape[1]-1]
 
#normalization
for i in range(x.shape[1]):
    x[:, i]=((x[:, i]-x[:,i].mean())/x[:, i].std())
    
x_shuffle, y_shuffle, y_data_shuffle=shuffle(x, y,y_data, random_state=0)
X_train, X_test, y_train, y_test,y_data_train, y_data_test = train_test_split(x_shuffle, y_shuffle, y_data_shuffle, test_size=0.2, random_state=0)
X_ver, X_test, y_ver, y_test, y_data_ver, y_data_test = train_test_split(X_test, y_test,y_data_test, test_size=0.5, random_state=0)

plt.figure(figsize=(14, 5))
polynomial_features = PolynomialFeatures(degree=1, 
                               include_bias=True,
                               interaction_only=False)
linear_regression = LinearRegression()
pipeline = Pipeline([("polynomial_features", polynomial_features),
           ("linear_regression", linear_regression)])
pipeline.fit(X_train, y_train)

# Evaluate the models using crossvalidation
scores = cross_val_score(pipeline, X_train, y_train, cv=3,
               scoring="r2")
print('Scores: {}'.format(scores))
print('Mean Absolute Error:', metrics.mean_absolute_error(y_ver, pipeline.predict(X_ver)))  
print('Mean Squared Error:', metrics.mean_squared_error(y_ver, pipeline.predict(X_ver)))  
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_ver,  pipeline.predict(X_ver))))

print('')
plt.scatter(y_test, pipeline.predict(X_test), label="Model")
  
  

#Another way
regressor = LinearRegression()  
regressor.fit(X_train, y_train) #training the algorithm
print('Score:', regressor.score(X_train, y_train))

#To retrieve the intercept:
print(regressor.intercept_)
#For retrieving the slope:
print(regressor.coef_)

y_ver_pred= regressor.predict(X_ver)
y_test_pred= regressor.predict(X_test)

df = pd.DataFrame({'Actual': y_test.flatten(), 'Predicted': y_test_pred.flatten()})
df.head(10)

print('Mean Absolute Error:', metrics.mean_absolute_error(y_ver, y_ver_pred))  
print('Mean Squared Error:', metrics.mean_squared_error(y_ver, y_ver_pred))  
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_ver, y_ver_pred)))
plt.scatter(y_ver_pred, y_ver)












